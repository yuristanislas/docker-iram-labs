
Architecture : 

-container docker apache-php and a container mariadb linked together.
-container linux bash to execute some .sh files


PHP

https://hub.docker.com/_/php?tab=description

src folder with php contents

 docker build -t labo-apache-php-image -f dockerfile-apache-php .
 
 docker run -p 80:80 -d --name my-labo-running-webserver labo-php-app


Bash
 
https://hub.docker.com/_/bash

docker build -t labo-bash-image -f dockerfile-bash .

docker run -it --rm --name my-labo-running-bash labo-bash-image


 interactive bash
 docker run -it --rm bash
MariaDB

https://hub.docker.com/_/mariadb

docker pull mariadb

FROM mariadb:latest
ENV MYSQL_ROOT_PASSWORD=Ir@mNetw0rkL@bs

shell access 

docker exec -it some-mariadb bash

logs docker 
docker logs some-mariadb

MYSQL_USER, MYSQL_PASSWORD
These variables are optional, used in conjunction to create a new user and to set that user's password. This user will be granted superuser permissions (see above) for the database specified by the MYSQL_DATABASE variable. Both variables are required for a user to be created.

Do note that there is no need to use this mechanism to create the root superuser, that user gets created by default with the password specified by the MYSQL_ROOT_PASSWORD variable.

When a container is started for the first time, a new database with the specified name will be created and initialized with the provided configuration variables. Furthermore, it will execute files with extensions .sh, .sql and .sql.gz that are found in /docker-entrypoint-initdb.d. Files will be executed in alphabetical order. You can easily populate your mariadb services by mounting a SQL dump into that directory and provide custom images with contributed data. SQL files will be imported by default to the database specified by the MYSQL_DATABASE variable.

docker run --name some-mariadb -v /my/own/datadir:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mariadb:tag

docker run --name some-mariadb -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mariadb:tag

docker container stop my-labo-running-DB && docker container rm my-labo-running-DB

In current folder
docker build -t labo-mariadb-image -f dockerfile-mariaDB . && docker run -p 3306:3306 -d --name my-labo-running-DB labo-mariadb-image

security improvement, change the port. 
 
 Docker compose ? 
 
 working on it!




reseau 1

mariaDB
APACHE/PHP
BASH

bridge ou autre ?? googeliser
Kali-linux.

https://docs.docker.com/network/bridge/