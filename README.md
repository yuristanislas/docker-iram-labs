Architecture : 

--network-labo
-container apache-php.
-container mariadb.
-container linux bash to execute some .sh files
--bridge-network
-container kali linux

*********************************************************
PHP/APACHE : https://hub.docker.com/_/php?tab=description


src folder with php contents


To launch without docker compose.

 docker build -t labo-apache-php-image -f dockerfile-apache-php .
 docker run -p 80:80 -d --name my-labo-running-webserver labo-php-app
*********************************************************


*********************************************************
Bash : https://hub.docker.com/_/bash
 
To launch without docker compose.

docker build -t labo-bash-image -f dockerfile-bash .
docker run -it --rm --name my-labo-running-bash labo-bash-image
*********************************************************



*********************************************************
MariaDB : https://hub.docker.com/_/mariadb

USER= admin-labo
PASSWORD= Ir@mNetw0rkL@bs
DATABASE= laboDB


docker exec -it some-mariadb bash

docker logs some-mariadb

docker volume create --name=mariadb-labo_data

docker container stop my-labo-running-DB && docker container rm my-labo-running-DB

In current folder
docker build -t labo-mariadb-image -f dockerfile-mariaDB . && docker run -p 3306:3306 -d --name my-labo-running-DB labo-mariadb-image

*********************************************************



*********************************************************
Kali-linux

--> currently the source list with intersting package is broken on every new kali image.
--> working on it.


*********************************************************

Docker compose ? 
 
cd --> /docker-iram-labs

docker-compose up --build
CTRL + C --> Waiting.
docker-compose down






